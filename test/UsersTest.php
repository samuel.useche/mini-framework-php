<?php
use PHPUnit\Framework\TestCase;

use App\Models\User;

class UsersTest extends TestCase
{
    public function test_it_properly_creates_users()
    {   
        $client = new \GuzzleHttp\Client();
        $faker = Faker\Factory::create();
        $user = new User();
        $dataInput =  [
            "username"=>$faker->name,   
            "email"=>$faker->email,
            "country"=>$faker->country,
            "password"=>$faker->password
        ];
        $response = $client->request('POST', 'http://127.0.0.1:8000/user/create', [
            'form_params' => $dataInput
        ]);

        $row = $user->findByEmail($dataInput['email']);
        $this->assertEquals($dataInput['username'], $row->name);
        $this->assertEquals(200,$response->getStatusCode());
        $this->assertTrue(true, true);
    }
}