<?php
use PHPUnit\Framework\TestCase;
use App\Models\User;

class AuthTest extends TestCase
{
    public function test_it_properly_login_users()
    {
        $client = new \GuzzleHttp\Client();
        $faker = Faker\Factory::create();
        $user = new User();
        $password = $faker->password;
        $user->name = $faker->name;
        $user->email = $faker->email;
        $user->country = $faker->country;
        $user->password = $password;
        $response = $client->request('POST', 'http://127.0.0.1:8000/login', [
            'form_params' => ['email'=>$user->email,'password'=>$password]
        ]);
        $this->assertEquals(200,$response->getStatusCode());
        $this->assertTrue(true, true);
    }
}