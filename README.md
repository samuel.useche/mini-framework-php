# Zinobe Backend Test - Samuel Useche

Hola y muchas gracias por revisar esta prueba

## Instrucciones de Instalacion

* Clone este repositorio ($ Git clone https://gitlab.com/jpvf/zinobe-test-repo.git)
* Cambiar a la rama develop ($ git checkout develop)
* Ir  al directorio ( $ cd /zinobe-test-repo)
* Correr el script de instalacion ($ ./install )
* ingrese en 127.0.0.1:8000

## Requerimientos

* Sitema operativo basado en linux
* GIT
* PHP 5.6
* MySql
