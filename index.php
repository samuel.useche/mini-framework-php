<?php 
require realpath(__DIR__.'/vendor/autoload.php');

use App\Core\Appkernel;


$app = new Appkernel();
$app->handle();