<?php namespace App\Core;

use App\Core\Helper; 
use App\Core\Middleware;
use App\Core\HttpRequest;

class Router extends HttpRequest
{
	const APP_FILE_PATH = "/app/Routes/.routes";
	
	public $routes;
	
	public function __construct()
    {
		$helper = new Helper;
		$base_dir = $_SERVER['DOCUMENT_ROOT'];
		$routes = $helper->readFile($base_dir . self::APP_FILE_PATH);

		foreach ($routes as $route) {

			list($method, $url, $controller) = $helper->formatFileData($route, " ");
			
			if($method == "GET") {
				$this->get($url, $controller);
			}
			else { 
				$this->post($url, $controller);
			}
		}
	}

	public function dispatch() 
	{
		list($class, $action, $middleware) = $this->parseIncomingRoute();
		
		if($this->authorize($middleware)) {

			$controller = new $class();
			
			if($id = $this->getIdParameter()) {
				$controller->$action($id);
			} else {
				$controller->$action();
			}

		} else {
		  $this->redirectTo('/');
		}
	}

	public function authorize($middleware) 
	{
		return (Middleware::checkAuth($middleware, $this->getSession()));
	}

	public function parseIncomingRoute()
	{
		$params = explode("@", $this->getAction($this->path(), $this->method()));

		return [$params[0], $params[1], $params[2]];
	}

	public function get($url, $controller)
	{
		$this->routes[$url]["GET"] = $controller;	  
	}

	public function post($url, $controller)
	{
		$this->routes[$url]["POST"] = $controller;	  
	}

	public function getAction($url, $method)
	{
		return $this->routes[$url][$method];
	}
}
  