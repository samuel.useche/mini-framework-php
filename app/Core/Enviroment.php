<?php 
namespace App\Core;

use App\Core\Helper; 

class Enviroment
{
	protected $env;

	public function __construct()
	{
		$helper = new Helper;
		$dataEnv = $helper->readFile(".env");

		foreach ($dataEnv as $data) {
			list($key, $value) = $helper->formatFileData($data, "=");
			
			$this->env[$key] = $value;
		}
	}

	public function getEnv($key)
	{
		return $this->env[$key];
	}
}