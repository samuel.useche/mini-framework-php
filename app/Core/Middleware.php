<?php

namespace App\Core;

use Lcobucci\JWT\Parser;
use App\Models\User;
use App\Core\HttpRequest; 

class Middleware
{
	public function checkAuth($middleware, $token)
	{
		
		if($middleware == "auth")
		{	
			if($token)
			{
				$user = new User();
				$token = (new Parser())->parse((string) $token);
	 			$userId = $token->getClaim('uid');
	 			$email = $token->getClaim('iss');

				return ($user->find($userId)) ? true : false;
			}
			
 			return false;
		}
		else
		{
			if($token)
			{
				HttpRequest::redirectTo('/user');
			}

			return true;
		}
	}
	
}