<?php namespace App\Core;

use Alcohol\ISO3166\ISO3166;

class Helper
{
	public function clearData($value = '')
	{
		return trim(trim($value,"\n"),"\r");
	}

	public function readFile($inputFile)
	{
		$output = array();
		$file = fopen($inputFile, "r");

		if($file) {
			while(!feof($file)) {
			  $read = fgets($file);
			  array_push($output,$read);
			}
		}

		return $output;
	}

	public function formatFileData($data, $separator)
	{
		$output = explode($separator, $data);

		foreach ($output as $key => $value) {
			$output[$key] = $this->clearData($value);
		}

		return $output;
	}

	public function tableUserTransform($users)
	{	
		$output = "";

		foreach ($users as $row) {
			$output = $output.'<tr><td>'.$row['name'].'</td><td>'.$row['email'].'</td><td>'.$row['country'].'</td></tr>';
		}

		return $output;
	}

	public function listCountries()
	{
		$output = "";
		$iso3166 = new ISO3166();

		foreach ($iso3166->getAll() as $country) 
		{
			$output = $output.'<option value="'.$country['name'].'">'.$country['name'].'</option>';
		}
		
		return $output;
	}
}