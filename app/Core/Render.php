<?php 
namespace App\Core;

class Render
{
	public function responseHtml($view, $data = NULL)
	{	

		$html = file_get_contents(__DIR__. '/../Views/'. $view. '.html');
		
		$array_data = ($data != NULL) ? (array) $data : []; 

		foreach ($array_data as $key => $value)  {
			$html = str_replace('%'. $key .'%', $value, $html);
		}

		echo $html;
	}
}