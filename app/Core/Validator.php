<?php namespace App\Core;


class Validator
{
    const REGEX_EMAIL = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
    const REGEX_PASSWORD =  '/^(?=.*\d)[0-9A-Za-z@#\-_$%^&+=§!\?]{6,20}$/';
    const REGEX_NAME = '/^(?=.*[0-9A-Za-z])[0-9A-Za-z]{3,20}$/';

    public function verifyError($regex,$value,$msg)
    {
        if(!preg_match($regex,$value))
        {
            echo "<script type='text/javascript'>alert('$msg'); window.location = '/user/register';</script>";
        }
    }
    public function ValidateUserData($data)
    {   
        $this->verifyError(SELF::REGEX_EMAIL,$data['email'],"Correo electronico no valido");
        $this->verifyError(SELF::REGEX_NAME,$data['name'],"Minimo del nombre son 3 caracteres");
        $this->verifyError(SELF::REGEX_PASSWORD,$data["password"],"La contraseña debe tener minimo 6 caracteres y tener un digito");
    }
}