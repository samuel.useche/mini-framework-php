<?php 

namespace App\Core;

use App\Core\Router;
use App\Core\Middleware;

class AppKernel
{
	  protected $router;

	  public function __construct()
    {
      session_start();
    	$this->router = new Router();
    }

    public function handle()
    {
      $this->router->dispatch();
    }
}