<?php 

namespace App\Core;

class HttpRequest
{

	public function path()
	{
		return (isset ($_SERVER["PATH_INFO"])) ? $_SERVER["PATH_INFO"] : "/";
	}

	public function method()
	{
		return $_SERVER["REQUEST_METHOD"];
	}

	public function attributes()
	{
		return (object) $_POST;
	}

	public function getSession()
	{
		return isset($_SESSION['access_token']) ? $_SESSION['access_token'] : false;
	}

	public function destroySession()
	{
		unset($_SESSION['access_token']);
	}

	public function startSession($token)
	{
		$_SESSION['access_token'] = $token;
	}

	public function redirectTo($path)
	{
		header('Location: '. $path);
	}

	public function getAttributes() 
	{
		return isset($_GET) ? $_GET : false;
	}

	public function getIdParameter() 
	{
		return isset($_GET['id']) ? $_GET['id'] : false;
	}
}