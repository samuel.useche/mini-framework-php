<?php 
namespace App\Controllers;

use App\Core\Render;
use App\Core\HttpRequest;
use Lcobucci\JWT\Builder;
use App\Models\User;
use App\Controllers\BaseController;

 class AuthController extends BaseController
 {
 	public function login()
 	{
 		Render::responseHtml('Auth/login');
 	}

 	public function singIn()
 	{
 		$data = $this->request->attributes();
 		$user_sys = $this->user->findByEmail($data->email);

 		if (password_verify($data->password,$user_sys->password)) {

		    $token = (new Builder())->setExpiration(time() + 3600)
		    			->setIssuer($user_sys->email) 
 						->set('uid', $user_sys->id)->getToken();
			 
			HttpRequest::startSession($token);
			HttpRequest::redirectTo('/user');
			 
		} else {
			$message = "Email o contraseña incorrectos";
			echo "<script type='text/javascript'>alert('$message'); window.location = '/';</script>";
		}
	 }
	 
 	public function logout()
 	{
 		HttpRequest::destroySession();
 		HttpRequest::redirectTo('/');
 	}
 }
