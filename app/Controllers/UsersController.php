<?php 
namespace App\Controllers;

use App\Core\Render;
use App\Core\Helper;
use App\Models\User;
use App\Core\HttpRequest;
use App\Controllers\BaseController;
use Lcobucci\JWT\Parser;

 class UsersController extends BaseController
 {
 	protected $help;

 	public function __construct()
    {
		parent::__construct();

    	$this->helper = new Helper;
	}
	
 	public function index()
 	{
 		$data = $this->request->getAttributes();
		 
		 if(isset($data['search'])) {
 			$users = $this->user->searchMultiple($data['search']);
		}
 		else {
 			$users = $this->user->all();
		}
		 
		$dataTable['table'] = $this->helper->tableUserTransform($users);
		 
		Render::responseHtml('User/index', $dataTable);
 	}

 	public function create()
 	{	
		 $data = $this->request->attributes();
		 
 		$this->user->name = $data->username;
 		$this->user->email = $data->email;
 		$this->user->password = $data->password;
		$this->user->country = $data->country;
		 
		$id = $this->user->save();
		 
 		HttpRequest::redirectTo('/user/show?id='. $id);
 	}

 	public function register()
 	{
		 $list->countries = $this->helper->listCountries();
		 
 		Render::responseHtml('User/register',$list);
 	}

 	public function show($id)
 	{	
 		$data = $this->request->attributes();
		 $userData = $this->user->find($id);
		 
 		Render::responseHtml('User/show',$userData);
 	}
 }