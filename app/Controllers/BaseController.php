<?php 
namespace App\Controllers;

use App\Models\User;
use App\Core\HttpRequest;

class BaseController
{
	protected $user;
	protected $request;

	public function __construct()
    {
    	$this->user = new User();
    	$this->request = new HttpRequest();
    }
}