<?php 
namespace App\Models;
use App\Core\Validator;

class User extends Model
{

	public $name;
	public $country;
	public $password;
	public $email;

	protected $validate;
	public function __construct()
	{
		parent::__construct("users");
		$this->validate = new Validator;
	}

	public function save()
	{
		$data = [
			"name"=> $this->name,
			"country"=> $this->country,
			"password"=> $this->password,
			"email"=> $this->email,
		];
		$this->validate->ValidateUserData($data);
		$data["password"] = password_hash($data["password"], PASSWORD_DEFAULT);
		return $this->insert($data);
	}

	public function find($id)
	{
		return $this->query('id',$id);
	}

	public function findByEmail($email)
	{
		return $this->query('email',$email);
	}

	public function all()
	{
		return $this->queryAll();
	}

	public function searchMultiple($value)
	{
		return $this->queryMultipleSearch($value);
	}
}