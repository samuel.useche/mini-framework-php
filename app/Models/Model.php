<?php 
namespace App\Models;

use Simplon\Mysql\Mysql;
use App\Core\Enviroment;

class Model
{
	protected $conector;
	protected $modelName;

	public function __construct($name)
	{
		$env = new Enviroment();

		$this->conector = new Mysql(
			$env->getEnv("DB_HOST"), 
			$env->getEnv("DB_USERNAME"),      
			$env->getEnv("DB_PASSWORD"),     
			$env->getEnv("DB_DATABASE") 
		);

		$this->modelName = $name;
	}

	public function query($parameter, $value)
	{
		$query = 'SELECT * FROM '.$this->modelName.' WHERE '.$parameter.' = :value';
		$conds = array('value' => $value);

		return (object) $this->conector->fetchRow($query, $conds);
	}

	public function queryAll()
	{
		$query = 'SELECT * FROM '.$this->modelName;
		
		return  $this->conector->fetchRowMany($query);
	}
	public function queryMultipleSearch($value)
	{
		$query = 'SELECT * FROM '.$this->modelName.' WHERE name LIKE "%'.$value.'%" OR email LIKE "%'.$value.'%"';
		
		return  $this->conector->fetchRowMany($query);
	}	
	public function insert($data)
	{
		return $this->conector->insert($this->modelName, $data);
	}

	public function update()
	{

	}

	public function delete()
	{

	}
}