    #!/bin/bash
    echo Ingrese la contraseña del usuario root de MySql
    read password
    echo Ingrese nombre de la base de datos 
    read database_name
    mysql -uroot -p${password} -e "CREATE DATABASE ${database_name} /*\!40100 DEFAULT CHARACTER SET utf8 */;"
    mysql -uroot -p${password} -e "USE ${database_name}; CREATE TABLE users(id INT NOT NULL AUTO_INCREMENT,email VARCHAR(20) UNIQUE NOT NULL,country VARCHAR(20),name VARCHAR(20),password VARCHAR(50) NOT NULL,PRIMARY KEY (id));"
    touch .env
    printf "DB_HOST=127.0.0.1\nDB_DATABASE=${database_name}\nDB_USERNAME=root\nDB_PASSWORD=${password}" > .env
    composer install
    echo "Iniciando Iniciando servidor"
    nohup php -S 127.0.0.1:8000 &
    echo Iniciando pruebas
    ./vendor/phpunit/phpunit/phpunit test/
    echo "Instalacion completa \0/"
    
